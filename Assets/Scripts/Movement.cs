﻿using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour
{
    public int moveSpeed; //int for how fas the palyer is moving
    public int jumpStr; //int for how much the player can jump

    Rigidbody2D body2d; // Rigidobdy2D declaration


    public bool canJump; //bool checking for players ability to jump

    public bool grounded; // bool for checking if player is on ground
    public Transform groundCheck;
    public float groundCheckRadius;
    public LayerMask whatIsGround; //layer mask for all the layers considered ground 

    // Use this for initialization
    void Start()
    {
        body2d = GetComponent<Rigidbody2D>(); //fetches the rigidobdy component needed for handling movement
    }

    void FixedUpdate()
    {
        //physics check for having good ground detection
        grounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, whatIsGround);
    }

    // Update is called once per frame
    void Update()
    {
        movement();
    }


    //Handles movement and jump stuff
    void movement()
    {
        if (Input.GetAxisRaw("Horizontal") != 0) //if horizontal buttons are pressed the player moves
        {
            body2d.transform.localScale = new Vector3(1f * Mathf.FloorToInt(Input.GetAxisRaw("Horizontal")), 1f, 1f); //handles player scale so it's facing the same way as it's walking
            body2d.velocity = new Vector2(moveSpeed * Time.deltaTime * Mathf.FloorToInt(Input.GetAxisRaw("Horizontal")), body2d.velocity.y);//handles player movemement     
        }


        //checks that the button is not being pressed and the player is grounded before stopping him.
        //Can add !slippery so that player keeps sliding on icy ground
        else if (Input.GetAxisRaw("Horizontal") == 0 && grounded)
        {
            body2d.velocity = new Vector2(0f, body2d.velocity.y); //stops the player if the button is not being pressed
        }
        if(Input.GetAxisRaw("Vertical") > 0 && grounded && canJump) //checks if player is allowed to jump;
        {
            body2d.AddForce(new Vector2(0f, jumpStr), ForceMode2D.Impulse); //adds force in impulse to make player jump.
            canJump = false;
        }
        else if(Input.GetAxisRaw("Vertical") == 0)//checks that jump button was released before player can jump again
        {
            canJump = true; //makes it so the player must release space to jump again
        }
        if (Input.GetAxisRaw("Vertical") < 0 )
        {
            gameObject.layer = 12;
            StartCoroutine(fallDown());
        }
    }

    void OnDrawGizmosSelected() //draws the gizmo representing the size of the groundCheck
    {
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(groundCheck.transform.position, groundCheckRadius);
    }
    IEnumerator fallDown()
    {
        yield return new WaitForSeconds(1f);
        gameObject.layer = 11;
    }
}
