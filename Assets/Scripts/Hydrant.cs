﻿using UnityEngine;
using System.Collections;

public class Hydrant : MonoBehaviour {

    public bool flipped; //if is flipped start spewing water

    ParticleSystem water;

	// Use this for initialization
	void Start () {
        water = GetComponentInChildren<ParticleSystem>();
	}
	
	// Update is called once per frame
	void Update () {
        if (flipped) water.enableEmission = true;
	}
}
