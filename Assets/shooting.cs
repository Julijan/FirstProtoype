﻿using UnityEngine;
using System.Collections;
using UnityEditor.iOS;

public class shooting : MonoBehaviour
{

    public bool canShoot;
    public bool shoot;

    public int dmg;

    public float cd;
    public float counter;

    public int ClipBullets;
    public int clipSizeMax;


    public GameObject ProjectilePrefab;
    public Transform projectileSpawn;
    public int projectileSpeed;

    void FixedUpdate()
    {
        if (canShoot) return;

        counter += Time.deltaTime;
        if (counter >= cd)
        {
            counter = 0;
            canShoot = true;
        }
    }

    void Update()
    {
        Shooting();


        if (ClipBullets > clipSizeMax)
        {
            ClipBullets = clipSizeMax;
        }
    }

    void Shooting()
    {
        if (Input.GetAxisRaw("Fire1") > 0 && canShoot && ClipBullets > 0)
        {
            Quaternion quaterninonRotation = projectileSpawn.rotation;
            //quaterninonRotation *= Quaternion.Euler(0, 0, Random.Range(-3, 5));

            var projectile = (GameObject)Instantiate(ProjectilePrefab, projectileSpawn.position + new Vector3(0f, 0f, -4f), quaterninonRotation);
            Rigidbody2D projectile2D = projectile.GetComponent<Rigidbody2D>();
            projectile2D.velocity = projectile.transform.right * projectileSpeed * transform.localScale.x;
            projectile.GetComponent<bulletScript>().dmg = dmg;

            ClipBullets -= 1;
            canShoot = false;
            if (!shoot)
            {
                shoot = true;

            }

        }
    }
}
